onsubmit = function(e){
    
    if(e.target.id === "newEvent"){
        let params = e.target ;
        
        createEvent(params.eventName.value, stringToInt(params.eventTile.value), params.eventType.value, getMultipleParam( params.eventArgs.value ),stringToInt( params.isModify.value));        
        params.eventName.value = "";        
        params.eventTile.value = "";
        params.eventArgs.value = "";
        params.isModify.value = false;

    }else if(e.target.id === "fullProjectForm"){
        let input = e.target.elements;
        let files = [];
        if(input.fullProject != undefined || input.tileFullProject != undefined || input.tileTypes != undefined){
            files[0] = input.fullProject.files;
            files[1] = input.tileFullProject.files;
            files[2] = input.tileTypes.files;

            
            setupProjectFull(files)
        }
    }else if(e.target.id === "newProject"){
        let input = e.target.elements;
        createNewProject(input.tileW.value,input.tileH.value,input.mapW.value,input.mapH.value,input.zIndex.value,input.scale.value,input.tileUrl.files);
    }else if(e.target.id === "loadMap"){
        let input = e.target.elements.localMapPath.value;
        
        changeMap(input);
    }else if(e.target.id === "projectSettings"){
        let input = e.target.elements;
        resizePicker(input.pickerMapH.value,input.pickerMapW.value);
    }else if(e.target.id === "addTile"){
        let input = e.target.elements;
        let optionalTile = undefined;
        
        if(input.from.value < 0 || input.to.value < 0){
            console.error("invalid data");
            
        }else{
            if(input.tile.value != ""){
                optionalTile = input.tile.value;
            }
            addTileOnRange(input.from.value,input.to.value,optionalTile);
            
        }
    }
    e.preventDefault();
}
function resizePicker(h,w){
    console.log("called picker change",h," : ",w);
    let updateP = false;
    if(h !== ""){
        if(h != 0){
            console.log("changed h");
            pickerMapH = parseInt(h);
            updateP = true;
        }
    }
    if(w !== "" ){
        if(w != 0){
            console.log("changed w");
            pickerMapW = parseInt(w);
            updateP = true;
        }
    }
    if(updateP){
        console.log("picker change detected");
        setPickerParams();
    }
}

//#region HTML CHANGERS 
function enableHtmlSection(id){
    let sec = document.getElementById(id);
    if(sec.style.display === "none"){
        sec.style.display = "block"
    }else{
        sec.style.display = "none"
    }
}
function changeTileSpan (tile){    
    let tilespan = document.getElementById("tile");
    //change for whatever the "null" block is
    if(tile === "null" ){
        return;
    }
    currentPickedTile = tile;
    if(tileTypes[tile] === undefined || tileTypes[tile] === null){
        return;
    }
    changeTileDisplayName();
}

function changeTileDisplayName(){
    document.getElementById("tile").innerText = tileTypes[currentPickedTile].name + " : " + currentPickedTile;
}

function setStartIndexSelect(){
    let start;
    let input = document.getElementById("startInput");
    start = currentPickedTile;
    input.value = start;
}
function setEndIndexSelect(){
    let end;
    let input = document.getElementById("endInput");
    end = currentPickedTile;
    input.value = end;    
}
function setEventTypeOnHtml(){
    let typesElement = document.getElementById("eventType");

    for(let i = 0; i < eventTypes.length; i++)
    {        
        option = document.createElement("option");
        option.text = eventTypes[i];
        option.value = eventTypes[i];
        typesElement.add(option);
    }
}
function setCurrentEventsOnHtml(){
    let currentEvElement = document.getElementById("currentEvents");
    currentEvElement.innerHTML = "";
    let eventKeys = events.keys(); //get map iterator with key values
    let currValue ;
    for(let i = 0; i < events.size; i++)
    {
        option = document.createElement("option");
        currValue = eventKeys.next().value; // i != event map key

        option.text = i + " : " +( Object.keys( events.get( currValue ) ) );
        option.value = currValue;
        currentEvElement.add(option);
    }
}
function readExistingEvent(event){
    //get the dropdown on which the data is stored
    let dropDown = document.getElementById("currentEvents");
    let readForm = document.getElementById("readEvent");

    //get the id from the dropdown, used for getting objects on event map
    let tempEventId = dropDown.value;    
    let tempValue = events.get( stringToInt( tempEventId ));
    let tempEventName = Object.keys(tempValue);

    readForm.children.eventName.value = tempValue[tempEventName].name;
    readForm.children.eventArgs.value = tempValue[tempEventName].args;
    readForm.children.eventType.value = tempEventName;
    readForm.children.eventTile.value = tempEventId;

}
function modifyExistingEvent(event){
        //get the dropdown on which the data is stored
    let dropDown = document.getElementById("currentEvents");
    let readForm = document.getElementById("newEvent");

    //get the id from the dropdown, used for getting objects on event map
    let tempEventId = dropDown.value;    
    let tempValue = events.get( stringToInt( tempEventId ));
    let tempEventName = Object.keys(tempValue);

    readForm.children.eventName.value = tempValue[tempEventName].name;
    readForm.children.eventArgs.value = tempValue[tempEventName].args;
    readForm.children.eventType.value = tempEventName;
    readForm.children.eventTile.value = tempEventId;
    //set this to the now old event tile if you are modifing an existing event instead of just creating a new one
    readForm.children.isModify.value = tempEventId;
}
function displayEventOverlay(color){
    let elements = document.getElementsByClassName("canvasOverlayBtn");
    let customColor = "#14dce647";
    if(color != undefined){
        customColor = color;
    }
    for(let i = 0 ; i < elements.length ; i++){
        
        elements[i].style.backgroundColor = customColor;
    }
    
}
function hideEventOverlay(){
    let elements = document.getElementsByClassName("canvasOverlayBtn");
        for(let i = 0 ; i < elements.length ; i++){
        
        elements[i].style.backgroundColor = "transparent";
    }
}
function closeMenuStartGame(){
    document.getElementById("configContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}
function helpSectionSwap(newBlockId,section){
    if(newBlockId === undefined ){
        console.log("new block id is undefined");
        
        return;
    }
    
    let oldVisible = document.getElementById(section).getElementsByClassName("block");
    let newVisible = document.getElementById(newBlockId);

    
    oldVisible[0].classList.add("none");
    oldVisible[0].classList.remove("block");
    
    
    newVisible.classList.remove("none");
    newVisible.classList.add("block");
}
function newProjectSwap(newBlockId,section){
    if(newBlockId === undefined){
        console.log("new block id is undefined");
        return;
    }
    let tempSection = document.getElementById(section);
    let tempNewBlock = document.getElementById(newBlockId);
    
    tempNewBlock.classList.remove("block");
    tempNewBlock.classList.add("none");

    tempSection.classList.remove("none");
    tempSection.classList.add("block");
    
}
function toggleTileNum(){
    showTileNum = !showTileNum;
}

//#endregion

//#region DOWNLOAD DATA 
function cleanDownload(downloadOrigin){
    console.log(downloadOrigin);
    document.getElementById(downloadOrigin).style ="display:none";    
    //TODO : this cant acces href, fix
    //URL.revokeObjectURL(href);
}

function downloadMap(mapData,filename,downloadOrigin){
    let map = JSON.stringify(mapData);
    const blob = new Blob([map], {type:"octet-stream"});
    let href = URL.createObjectURL(blob);
    
    let a = Object.assign(document.getElementById(downloadOrigin), {
        href, 
        style:"display:block",
        download:filename+".json"
    });
}

function exportProject(downloadOrigin){
    let name = document.getElementById("filename").value;
    if(name === undefined || name === null || name === ""){
        name = "Project";
    }

    let proj = {};
    let settings = {
        "tileSetUrl" : tileSetURL,
        "tileW" : tileW, 
        "tileH" : tileH,
        "scale" : scale,
        "mapW" : mapW, 
        "mapH" : mapH,
        "zLevels" : zLevels      
    }
    //turn event map into json and save it
    let localEvents = mapToJson(events);
    //use local event to get json event keys
    let eventKeys = Object.keys(localEvents)
    let exportEvent = {};
    let exportEventKeys = [];
    let exportEventContents = [];
    //cycle thru all the events and get their data
    for(let i = 0 ; i < Object.keys(localEvents).length ; i++){
        //gets name of the event type
        let tempData = Object.keys ( localEvents [ eventKeys[i] ] ) ;

        //checks if the event type is already on object
        //if it is, add new content
        //if it is not, add event type and then content
        if( arrayContains(tempData[0], exportEventKeys) ){
            exportEventKeys.indexOf(tempData[0])
            
            
            exportEvent[ tempData[0] ].push( Object.entries( localEvents [ eventKeys[i] ] )[0][1] ) ;
        }else{
            
            exportEventKeys.push(tempData[0])
            exportEvent[tempData[0]] = [];
            exportEvent[ tempData[0] ].push( Object.entries( localEvents [ eventKeys[i] ] )[0][1] ) ;
        }
    };
    
    
    proj.settings = settings;
    proj.map = gameMapData;
    proj.zContent = mapToJson(zContents);
    proj.events = exportEvent;
    downloadMap(proj,name,downloadOrigin);
    updateSaveState("project",false);
}

function mainDownload(downloadOrigin){
    let name = document.getElementById("filename").value;
    if(name === undefined || name === null || name === ""){
        name = "map";
    }
    console.log("name: ",name);
    downloadMap(gameMapData,name,downloadOrigin)
}

function downloadTypes(downloadOrigin){
    let name = "tileTypes";
    
    downloadMap(tileTypes,name,downloadOrigin);
    updateSaveState("tileTypes",false);
}

//#endregion

//#region EVENTS
function generateEventsFromVar(){
    if([Object.keys(EventList)][0].length === 0){
        return;
    }
    
    for(j = 0; j < Object.keys(EventList).length ; j++){
        for(let i = 0 ; i < EventList[ eventTypes[j] ].length ; i++){
            let obj = {};
            obj = { [Object.keys(EventList)[j]] : EventList[ eventTypes[j] ][i] };
            addEvent(EventList[ eventTypes[j] ][i].pos, obj);
        }
    }
}
//#endregion

function setScrollBarSize(scrollID, tileAmmount,paramTileW,paramTileH,paramMapH){
    let localTileW, localTileH;
    let scrollBar = document.getElementById(scrollID);
    if(paramTileW === undefined){
        localTileW = tileW
    }
    if(paramTileH === undefined){
        localTileH = tileH
    }
    

    scrollHeight = (ammountOfTiles / paramMapH) / paramMapH; //this assumes H and W are the same
    scrollPelletSize = Math.floor(scrollBar.getBoundingClientRect().height / scrollHeight);
    scrollBar.children[0].style.height = scrollPelletSize + "px";


}

function scrollWithClick(scrollID,clickPos){
    let scrollBar = document.getElementById(scrollID);
    //sum half of the pellet size
    let pos = clickPos - (scrollPelletSize/2);
    let percentage = ( clickPos / scrollBar.getBoundingClientRect().height).toFixed(5) * 100;
    
    movePickerScroll(scrollID,pos);
    scrollPicker(percentage);
}

function wheelScrollMove(scrollID,percentage){
    let scrollBar = document.getElementById(scrollID);
    let percent = Math.ceil( (scrollBar.getBoundingClientRect().height)  * (percentage / 100) );
    
    movePickerScroll(scrollID, percent)
}

function movePickerScroll(scrollID,position){
    let scrollBar = document.getElementById(scrollID);
    scrollBar.children[0].style.transform = "translateY("+position +"px)";
    
}

function scrollPicker(percentage){
    initTile = Math.floor( pickerMapTotal * (percentage/100));
    initTileTotalW = initTile * pickerMapW;
    endTile = (initTile * pickerMapH) + (pickerMapW * pickerMapH);
    drawPicker();
}