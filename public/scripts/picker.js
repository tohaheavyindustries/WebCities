var tileset = null;
tileset = new Image();


//size of map on tiles also take on count the scale of the map
let pickerMapW = 8, pickerMapH = 8,pickerMapTotal;

let scrollHeight, scrollPelletSize;

let initTile , endRow ;
let endTile , initTileTotalW ;

let ammountOfTiles;

let blockPickerMap = [];

function setPickerParams(){
    initTile = 0, endRow = 0;
    endTile = 0, initTileTotalW = 0;

    ammountOfTiles = 0;

    blockPickerMap = [];

    tileset.src = tileSetURL;
    pickerCanvas = document.getElementById("blockPick");
    pctx = pickerCanvas.getContext("2d");
    let canvasSize = setCanvasSize(pickerMapW,pickerMapH,tileW,tileH,scale);

    pickerCanvas.width = canvasSize[0];
    pickerCanvas.height = canvasSize[1];
    picker();
}

function picker(){
    ammountOfTiles = Object.keys(tileTypes).length - 1;
    pickerMapTotal = Math.ceil(ammountOfTiles / pickerMapW);
    endTile = (pickerMapW * pickerMapH) ;
    
    setScrollBarSize("customScrollBar",ammountOfTiles,tileW,tileH,pickerMapH);
    /*
    console.log("picker w/h : ",pickerMapW,pickerMapH);
    console.log("tile am: ",ammountOfTiles);
    console.log("picker map total: ",pickerMapTotal);
    console.log("endtile: ",endTile);
    */
    createTiles();
    //blockPick
    
    pctx.scale(scale,scale);
    pctx.imageSmoothingEnabled = false;
    
    var tile;
    drawPicker();
}

function drawPicker(){
    pctx.clearRect(0,0, (pickerMapW * tileW) * scale, (pickerMapW * tileH) * scale);
    
    for(let y = 0; y < pickerMapH;y++){
        for(let x = 0; x < pickerMapW;x++){            
            tile = tileTypes[(blockPickerMap[toIndex(x,y,pickerMapW)]) + initTileTotalW];
            
            if(tile === undefined || tile === null){
                //if there is no block just leave a blank space
                tile = tileTypes[0];
            }
            
            pctx.drawImage(tileset,
                tile.sprite[0].x, tile.sprite[0].y, tile.sprite[0].w, tile.sprite[0].h,
                (x*tileW),  (y*tileH),
                tileW, tileH);
            //if tile is the currently picked, add an overlay to mark
            if ( blockPickerMap[toIndex(x,y,pickerMapW)]  + initTileTotalW  === currentPickedTile){
                
                pctx.globalAlpha = 0.2;
                pctx.beginPath();
                pctx.fillStyle = "#ff0000";
                pctx.rect((x*tileW),(y*tileH),tileW, tileH);
                pctx.fill();
                pctx.globalAlpha = 1;
            }
            //If tile is animated add a black bar and text to signal so
            if(tile.animated){
                    pctx.beginPath();
                    pctx.fillStyle = "#000000";
                    pctx.rect((x*tileW),(y*tileH)+tileH / 1.2,tileW , tileH/4);                        
                    pctx.fill();
                    pctx.beginPath();
                    pctx.font = "bold 5pt sans-serif";
                    pctx.fillStyle = "#ff0000";
                    pctx.fillText("Anim",(x*tileW) + (tileW/5) , (y*tileH) + tileH);
            }
        }
    }
}

function moveTiles(){
    
}

function createTiles(){
    for(i = 0; i < ammountOfTiles; i++){
        blockPickerMap[i] = i;
    }
}