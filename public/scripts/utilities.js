

//#region DOM Events
onwheel = function(e){
    if (elementHover != "blockPick"){
        return;
    }
    let direction = null;
    
        if(e.deltaY < 0){
            direction = "Up";
            if(initTile > 0){
                initTile = initTile - 1;
                initTileTotalW = initTileTotalW - pickerMapW;
                endTile = endTile - pickerMapW;
                wheelScrollMove("customScrollBar", (initTile / pickerMapTotal).toFixed(5) * 100);
                
                drawPicker()
            }
        }else{
            direction = "Down";
            if(endTile < ammountOfTiles){
                initTile = initTile + 1;
                initTileTotalW = initTileTotalW + pickerMapW;
                endTile = endTile + pickerMapW;
                wheelScrollMove("customScrollBar", (initTile / pickerMapTotal).toFixed(5) * 100);
                
                drawPicker()
            }
        }
}
onkeydown = function(e){
    
    if(e.key === "Shift" && shiftPressed === false){
        
        shiftPressed = true;
    }
}
onkeyup = function(e){
    
    if(e.key === "Shift"){
        
        shiftPressed = false;
    }
}
oncontextmenu = function(e){
    if(elementHover === null){return;}
    e.preventDefault();
}
onmousedown = function(e){
    getClickContext(e);
}

onload = function(e){
    const fileSelector = document.getElementById("fullProject");
    fileSelector.addEventListener("change", (e) => {
        let file = e.target.files;
        
        //setupProjectFull(file);
    });
}


//#endregion

//#region MOUSE CONTROLS 
function mouseEnterCanvas(id){
    //console.log("enter: ",id);
    elementHover = id;
}

function mouseLeaveCanvas(id,event){
    
    if(event != undefined || event != null){        
        if( event.relatedTarget != null && event.relatedTarget.className === "canvasOverlayBtn"){
            return;
        }
    }
    
    //console.log("leave: ",id);
    elementHover = null;
}

// THIS SHOULD BE CHANGED FOR NON EDITOR VERSION
function getClickContext(e){
    //console.log(elementHover);
    if(elementHover === "game"){
        canvasOffset = gameCanvas.getBoundingClientRect();
        let posx = (e.pageX - canvasOffset.left)/scale;
        let posy = (e.pageY - canvasOffset.top)/scale;
        let tiles = pixelToMap(posx,posy, tileW,tileH);

        if(shiftPressed === true){
            hasEvent(tiles);
            return;
        }
        
        if(getMouseBtn(e) === "right"){
            //TODO: make a context menu appear if there is multiple layers
            getLayersAmmount(tiles[0],tiles[1], mapW)
            return;
        }else if(getMouseBtn(e) === "mid"){
            
            changeTileSpan ( gameMapData[ toIndex(tiles[0],tiles[1],mapW) ] );
            return;
        }
        
        addNewTile(tiles[0],tiles[1], mapW);
    }else if(elementHover === "blockPick"){        
        canvasOffset = pickerCanvas.getBoundingClientRect();
        let posx = (e.pageX - canvasOffset.left)/scale;
        let posy = (e.pageY - canvasOffset.top)/scale;
        let tiles = pixelToScrollMap(posx,posy, tileW,tileH);
        //TODO : fix this crap, this code shouldn't check the size of the current tiles +2 (null && 0), it should check if the actual tile exist
        //console.log(Object.keys(tileTypes).length, " , ",toIndex(tiles[0],tiles[1], pickerMapW) +2 );
        if(ammountOfTiles < (toIndex(tiles[0],tiles[1], pickerMapW))  ){
            //console.log("no such tile");
            return;
        }
        changeTileSpan(toIndex(tiles[0],tiles[1], pickerMapW));
        drawPicker();
    }else if(elementHover === "customScrollBar"){
        console.log(elementHover, e.pageY - e.target.getBoundingClientRect().top);
        if(e.target.id === "scrollPellet"){
            console.log("nothing");
            return
        }
        scrollWithClick(elementHover,e.pageY - e.target.getBoundingClientRect().top);
        return;
    }else{
        //console.log("no hover, exit");
        return;
    }
}

function getMouseBtn(e){
    //Left
    //console.log("pressed: ",e.button);
    if(e.button === 0){
        
        return "left";
    }else if(e.button === 2){
        
        
        return "right";
    }else{
        
        return "mid";
    }
}

//#endregion

//#region FETCH DATA 
function fetchFile(filePath){
    let tempSave;
    
    fetch(filePath)
    .then(res => {
        
        return res.json();
    })
    .then(jsonData => {
        tempSave = jsonData;
    }).catch(function(error){
        console.error(error);
        window.alert("Error proyect parameters couldnt be loaded",error);
    });
    
}
function fetchNewMap(filePath){
    
    fetch(filePath)
    .then(res => {        
        return res.json();
    })
    .then(jsonData => {
        //fetch should return data, and this beahaviour must happen on another function
        changeGameMap(jsonData.map);

        if(jsonData.zContent){
            
            setNewLayers(jsonData.zContent);
        }else{
            zContents = new Map();
        }

        EventList = new Map();
        events.clear();

        if(jsonData.events){
            EventList = jsonData.events;
            generateEventsFromVar();
        }
        buildOverlayButtons();
        setCurrentEventsOnHtml();

    }).catch(function(error){
        console.error(error);
        window.alert("Error proyect parameters couldnt be loaded",error);
    });
    
}


//#endregion

//#region CONVERT DATA 
function indexToPx(index, mapw){
    if(mapw === undefined || mapw === null){
        mapw = mapW
    }
    let x = index % mapw;
    let y = Math.floor(index / mapw);
    return[x,y];
}

function pixelToMap(x ,y, tileW,tileH){        
        return [Math.floor(x / tileW) , Math.floor(y / tileH)]
}

function pixelToScrollMap(x ,y, tileW,tileH){        
        return [Math.floor(x / tileW) , Math.floor((y + initTile * tileH) / tileH)]
}

function toIndex(x, y, mapW)
{
	return((y * mapW) + x);
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

function mapToJson(map){
    let TempObject;
    TempObject = Object.fromEntries(map);
    return TempObject;
}

function jsonToMap(json,forStartFrom){
    let tempMap = new Map();
    for(i = forStartFrom; i <= Object.keys(json).length; i++){
        tempMap.set(i,json[i]);
    }
    return tempMap;
}
function stringToInt(string){
    return string * 1;
}
//#endregion

//#region TILES MODIFIERS

function addLayeredObj(tileNum,content,layer){
    let currentLayer;
    let cont = {};
    
    if(layer === undefined || layer  === null || layer === ""){
        currentLayer = drawingOnLayer;
    }else{
        currentLayer = layer;
    }
    //console.log("added",currentLayer,  "tile",content);
    
    if(zContents.has(currentLayer) && zContents.get(currentLayer) != undefined){
        cont = zContents.get(currentLayer);
        
    }
    
    cont[tileNum] = content
    zContents.set(currentLayer,cont);
    if(!firstBoot){
        updateSaveState(saveTypes[1],true);
    }
}
function removeLayeredObj(tileNum,layer){
    let tempLayers;
    tempLayers = zContents.get(layer);
    delete tempLayers[tileNum]

    //zContents.set(tempLayers);
}
function addTileToMap(x,y, mapW){
    gameMapData[ toIndex(x,y, mapW) ] = currentPickedTile;
    if(!firstBoot){
        updateSaveState(saveTypes[1],true);
    }
}
function addTileToMapFromPos(tilePos,tile){
    if(tile === undefined){
        tile = currentPickedTile;
    }
    gameMapData[ tilePos ] = tile;
    if(!firstBoot){
        updateSaveState(saveTypes[1],true);
    }
}
function addNewTile(x,y,mapW){
        if( drawingOnLayer === 0){
            addTileToMap(x,y, mapW)
            
        }else{
            //saves the code of the tile and position.
            addLayeredObj(toIndex(x,y, mapW) , currentPickedTile);
            
        }
}
function removeTile(x,y,mapW){    
    gameMapData[ toIndex(x,y, mapW) ] = 0;
}
function addTileOnRange(from,to,tile){
    let localTo;
    if(tile === undefined){
        tile = currentPickedTile;
    }
    if(to > (mapW * mapH)){
        localTo = (mapW * mapH);
    }else{
        localTo = to;
    }
    for(from ; from <= localTo ; from++){
        addTileToMapFromPos(from,tile)
    }
}
//#endregion

//#region SET PROJECT DATA
function setupProjectFull(file){
    
    let projDone,tileDone;
    let fileRead = new FileReader();
    let tileTypeRead = new FileReader();
    let gotData;
    let tileTypeData;

    async function imgres(){
        let a = await readFileImg(file[1][0])
        if(projDone === true && tileSetURL != undefined && tileDone === true ){
            
            buildProyect();
        }
    }
    imgres();

    fileRead.readAsText(file[0][0]);
    fileRead.onload = function() {
        gotData = fileRead.result;
        gotData = JSON.parse(gotData);
        
        tileW = gotData.settings.tileW; 
        tileH = gotData.settings.tileH;
        scale = gotData.settings.scale;
        mapW = gotData.settings.mapW;
        mapH = gotData.settings.mapH;
        
        zLevels = gotData.settings.zLevels;
        gameMap = gotData.map;

        EventList = gotData.events;

        tempLevels = gotData.zContent;
        projDone = true;
        if(projDone === true && tileSetURL != undefined && tileDone === true ){
            
            buildProyect();
        }
    }
    fileRead.onerror = function() {
        alert(fileRead.error);
        return;
    };

    tileTypeRead.readAsText(file[2][0]);
    tileTypeRead.onload = function() {
        tileTypeData = tileTypeRead.result;
        tileTypeData = JSON.parse(tileTypeData);

        tileTypes = tileTypeData;
        tileDone = true;
        if(projDone === true && tileSetURL != undefined && tileDone === true ){
            
            buildProyect();
        }
    }
    tileTypeRead.onerror = function() {
        alert(tileTypeRead.error);
        return;
    };
    
    
}
function readFileImg(file){
    let imageRead = new FileReader();
    return new Promise(resolve => {
    imageRead.readAsDataURL(file);
    imageRead.onload = function (){
        tileSetURL = imageRead.result;
        
        resolve (true);
    };
    imageRead.onerror = function() {
        alert(imageRead.error);

        resolve (false);
    };
    });
}

function setCanvasSize(mapW,mapH,TilesW,TilesH,scale){
    let canvasW = (mapW * TilesW) * scale;
    let canvasH = (mapH * TilesH) * scale;
    
    return [canvasW,canvasH];
}

function createTileSetFromImg(){
    let ammounTileW = tileset.width / tileW;
    let ammounTileH = tileset.height / tileH;
    let totalTiles = ammounTileH * ammounTileW;

    for(let y = 0; y < ammounTileH ; y++){
        for(let x = 0; x < ammounTileW ; x++){
            tileTypes[toIndex(x,y,ammounTileW)] =  { name:"name "+ toIndex(x,y,ammounTileW) , floor:floorType.path, 
                sprite:[{x:x*tileW,y:y*tileH,w:tileW,h:tileH}] , colour:"#999999"
            }
        }
    }
}

function generateGameMap(mapW,mapH){
    let newMap = [];
    for (i = 0; i < mapW*mapH; i++){
        newMap[i] = 0;
    }
    return newMap;
}

function setLayers(layers){
    let layersTotal;
    let selectElement = document.getElementById("layerSelect");
    let option;
    if(layers != undefined){
        layersTotal = layers;
    }else{
        layersTotal = zLevels;
    }
    for(let i = 1; i <= layersTotal; i++)
    {
        if(i === 1){
            option = document.createElement("option");
            option.text = "Layer " + (i - 1) + " (Default)";
            option.value = i - 1;
            selectElement.add(option);
        }
        zContents.set(i);
        option = document.createElement("option");
        option.text = "Layer " + i;
        option.value = i;
        selectElement.add(option);
    }
}

function modifyTileType(tile,value){
    let tempTile = tile;
    let tempVal = document.getElementById("zindexInput").value;
    if(tempTile === undefined || tempTile === null || tempTile === ""){
        if(tempTile != 0){
            tempTile = currentPickedTile;
        }else{
            console.log("cant modify that tile");
        }
    }

    if(tileTypes[tempTile] === undefined){
        console.log("invalid tile");
    }else{
        tileTypes[tempTile].zIndex = stringToInt( tempVal );
    }
    
}

function modifyTileTypeRange(tile){
    let tempValStart = document.getElementById("startInput").value;
    let tempValEnd = document.getElementById("endInput").value;
    let index = document.getElementById("zindexRangeInput").value;
    if(tempValStart === undefined || tempValStart === null || tempValEnd === 0 || tempValEnd === undefined || tempValEnd === null){
        console.error("invalid data",tempValStart,tempValEnd);
        return;
    }
    if(index === ""){
        console.log("Index is empty");
        return
    }
    
    for(let i = tempValStart; i <= tempValEnd; i++){
        if(tileTypes[i] === undefined){
            console.log("invalid tile");
        }else{
            tileTypes[i].zIndex = index;
            console.log(tileTypes[i].zIndex);
        }
    }
    
}

function createEmptyProject(mapw,maph,tilesh,tilesw){
    mapW = mapw;
    mapH = maph;
    tilesH = tilesh;
    tilesW = tilesw;

    gameCanvas = document.getElementById("game");
    ctx = gameCanvas.getContext("2d");
}

function createNewProject(tilew,tileh,mapw,maph,zindex,scaleLocal,img){    
    if(scaleLocal === undefined){
        scale = 1;
        console.warn("scale not set");
    }else{
        scale = scaleLocal;
    }
    tileW = tilew;
    tileH = tileh;
    mapW = mapw;
    mapH = maph;
    zLevels = zindex;

    gameMap = generateGameMap(mapW,mapH);
    
    async function imgres(){
        let a = await readFileImg(img[0])
        if(a === true){
            
            buildProyect();
        }else{
            console.error("error img couldnt be loaded");
        }
    }
    imgres();
}
//#endregion

//#region HAS DATA OR GET DATA
function hasEvent(tile){
    let checkTile = toIndex(tile[0],tile[1],mapW);
    
    if(events.has(checkTile)){
        findEvent( events.get(checkTile) );
    }
}
function findEvent(event){
    //console.log("event lookup");
    if( event["goToPage"] != undefined ){
        goToPage(event["goToPage"]["args"][0],event["goToPage"]["args"][1]);
    }else if( event["openWindow"] != undefined  ){
        openNewWindow();
    }else if( event["changeMap"] != undefined ){
        
        changeMap(event["changeMap"]["args"][0]);
    }else{
        console.error("event not found",event);
    }
}
function getLayersAmmount(x,y, mapW){
    let tile = toIndex(x, y, mapW );
    
    for(i = zLevels; i >= 1; i-- ){
        
        if(zContents.get(i) !== undefined){
            if( zContents.get(i)[tile] != undefined ){
                
                removeLayeredObj(tile,i);
                updateSaveState(saveTypes[1],true);
                return;
            }
        }
        if(i === 1){
            
            removeTile(x,y,mapW);
            updateSaveState(saveTypes[1],true);
            return;
        }
    }
}
function arrayContains(item, array){
    return (array.indexOf(item) > -1);
}
//#endregion

//#region Events
function createEvent(eventName,eventTile,eventType,event,isModify,pointer){
    let tempEvent = {};
    let tempParams = [];
    
    if( typeof(isModify) === "number"){
        //is modify is usually false, when changed turns into a number, that being, the tile where this event previously was
        //then removes it so there is no duplicated events on different tiles
        events.delete(isModify)
    }
    for(let i = 0; i < event.length ; i++){
        if(event[i] == "true" || event[i] == "false"){
            tempParams[i] = ( event[i] == "true" );
        }else{
            tempParams[i] = event[i];
        }
    }
    
    tempEvent = {[eventType]: {"pos":eventTile,"args":tempParams,"name":eventName}};
    
    addEvent(eventTile,tempEvent);
    buildOverlayButtons();
}
function addEvent(tileCoords,event){    
    events.set(tileCoords,event);    
    setTimeout(setCurrentEventsOnHtml,100)
    if(!firstBoot){
        updateSaveState(saveTypes[0],true);
    }
}

//#endregion

function getMultipleParam(parameter){
    let param = parameter;
    let len = param.length;
    let tempParams = [];
    tempParams[0] = "";
    let ammountOfParams = 0;

    for (i = 0; i < len ; i++){
        if(param[i] === ","){
            ammountOfParams ++;
            tempParams[ammountOfParams] = "";
        }else{    
            tempParams[ammountOfParams] += param[i]
        }
    }
    
    return tempParams;
}

function changeGameMap(newMap){
    let temp;
    if(newMap != undefined){
        temp = newMap
    }else{
        temp = gameMap;
    }
    
    gameMapData = temp.slice(0);
}
function setNewLayers(newLevels){
    let temp;
    if(newLevels != undefined){
        temp = newLevels;
    }else{
        temp = tempLevels;
    }
    zContents = jsonToMap(temp,1);
}

function changeMap(newMapPath,transition){
    //this is async, make the set functions wait for fetch
    if(transition != undefined){
        console.log("transition ",transition," was called. . .");
    }
    fetchNewMap(newMapPath);
}

// MADE ALTERNATIVE VERSION, THAT CREATES A WINDOW ON HTML ASKING THE USER IF THEY WANT TO GO TO ANOTHER PAGE, (WITH FLAVOR TEXT)
function goToPage(link,newTab){
    
    let a = document.createElement('a');
    a.href = link;
    if (!newTab){
        a.setAttribute('target');
    }else{
        a.setAttribute('target','_blank');
    }
    a.click();
}
// the positions
function openNewWindow(winName,winContent,winWidth,winHeight,newWinURL,leftPos,topPos){
    let newWindow, newWindowUrl = "";
    let winParams = "width="+winWidth+","+"height="+winHeight;
    if(leftPos != undefined ){
        winParams +=","+"screenX="+leftPos;
    }
    if(topPos != undefined){
        winParams += ","+"screenY="+topPos;
    }
    if(newWinURL != undefined){
        
        newWindowUrl = newWinURL
    }
    //Window URL not working for some reason
    newWindow = window.open(newWindowUrl,winName,winParams)
    newWindow.document.write(winContent);
}

function isColliding(myElement,otherElement){
    let originX = myElement.x;
    let originY = myElement.y;
}

/*
    What is this? there is no easy way to make collisions or hovers on canvas, since its contents are not 
    elements on themselves. you can make a manual collision detection, onmousemove check if mouse is inside
    tile, but probalby is going to be quite demanding, and with a big ammount of events or clickable effects
    is going to tank the browser, probably you can make a custom onmousemove, doing some infinite interval
    so its less efficient but more performative, also you could try to divide de screen in 4 sections and
    define on wich section of the screen each event should be, so instead of checking all of the posible
    combinations you only see the ones on that cuadrant
    But im too lazy to test all of that,
    so im putting some <button> elements and making them invisible so i can have a pointer change effect
    i know, really stupid and confusing, but the feeling of the page is everything in this scenario
*/
function buildOverlayButtons(){
    let overlay = document.getElementById("canvasOverlay");
    let tempButton, currValue, currentPos,style;
    let eventKeys = events.keys();
    let eventParams ;

    if(overlay.hasChildNodes() === true){        
        overlay.innerHTML = "";
    }

    for(let i = 0; i < events.size ; i++){
        currValue = eventKeys.next().value;
        currentPos = indexToPx(currValue,mapW);
        eventParams = events.get(currValue);
        
        
        style = "left:"+(currentPos[0]*tileW) * scale +"px";
        style += ";top:"+(currentPos[1]*tileH) * scale+"px";
        style += ";width:" + tileW * scale+ "px";
        style += ";height:" + tileH * scale+ "px";
        // check if the event has a custom pointer or none at all
        //if( eventParams[ Object.keys( eventParams ) ].args.pointer != undefined ){
        //    console.log("test");
        //}
        tempButton = document.createElement('button');
        tempButton.title = eventParams[ Object.keys( eventParams ) ].name; 
        tempButton.className = "canvasOverlayBtn";
        tempButton.id = "overlayBtn"+currValue;
        tempButton.style = style;

        tempButton.setAttribute('onclick', 'getClickContext(event);');
        


        overlay.appendChild (tempButton);
    }
}

function changeTileName(inputElID){
    let newName = document.getElementById(inputElID).value;
    if(newName === undefined || newName === null || newName === ""){
        return;
    }
    if(currentPickedTile < 0 ){
        return;
    }
    tileTypes[currentPickedTile].name = newName;
    document.getElementById(inputElID).value = "";
    changeTileDisplayName();
    updateSaveState(saveTypes[0],true);
}

function updateSaveState(elementToUpdate,state){
    console.log("entered update");
    //element to update is what has been modified, state refers to, already saved(false) or unsaved(true)
    if(state === true){ //save needs to be done
        for(let i = 0; i < unsavedOn.length ; i++){
            if(unsavedOn[i] === elementToUpdate){
                //ignore
                return;
            }
        }
        console.log("got new EL",elementToUpdate);
        unsavedOn.push(elementToUpdate);
        hasUnsavedChanges = true;
        saveStatePopUp(true);
    }else if(state === false){
        
        for(let i = 0; i < unsavedOn.length ; i++){
            if(unsavedOn[i] === elementToUpdate){
                unsavedOn.splice (unsavedOn.indexOf(elementToUpdate),1);
            }
        }
        if(unsavedOn.length <= 0){
            hasUnsavedChanges = false;
            saveStatePopUp(false);
        }
    }
    //UPDATE UI
    
}

function saveStatePopUp(mustSave){
    let unsaved = document.getElementById("unsaved");
    let saved = document.getElementById("saved");
    
    console.log(Array.from(unsavedOn));
    if(mustSave){
        let tempContent = "You have Unsaved changes for " + unsavedOn;
        unsaved.innerHTML = tempContent;
        unsaved.style.display = "block";
    }else{
        unsaved.style.display = "none";
    }
    
}